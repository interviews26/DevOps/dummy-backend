# Project Dockerization

**Task:** Create a Dockerfile (dockerize) the project so we can start the project using:

```shell
$  docker run \
   --name dummy-backend-container -it \
   --network sedaq-training \
   -p 8082:8082 \
   dummy-backend-image:1.0.0
```
