package org.but.feec.service.mappers;

import org.but.feec.api.AddressDetailedViewDto;
import org.but.feec.data.entity.Address;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AddressMapper {

    AddressDetailedViewDto mapToDetailedView(Address address);
}
